import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSun } from '@fortawesome/free-solid-svg-icons'
import { faTint } from '@fortawesome/free-solid-svg-icons'
import { faCloud } from '@fortawesome/free-solid-svg-icons'
import { faSnowflake } from '@fortawesome/free-solid-svg-icons'
import fontawesome from '@fortawesome/fontawesome'
import '@fortawesome/fontawesome-free'
import {Container, Row, Col} from 'reactstrap'

fontawesome.library.add(faSun)
fontawesome.library.add(faTint)
fontawesome.library.add(faCloud)
fontawesome.library.add(faSnowflake)

export default class LocalWeatherText extends React.Component{



render() {

var sun = {
    backgroundColor: '#FFA500',
    color: 'white',
    fontSize: '50px'
}

var cloud = {
    backgroundColor: '#00BFFF',
    color: 'white',
    fontSize: '50px'
}

var rain = {
    backgroundColor: '#2F4F4F',
    color: 'white',
    fontSize: '50px'
}

 if (this.props.weather === 'Clear') {
            return (
		<div>
            <Container fluid>
            <Row style = {{textAlign: 'center', maxHeight: '25%', marginTop: '12rem'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h1 style = {{color: 'white'}}>{this.props.city}</h1>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h2 style = {{color: 'white'}}>{this.props.weather}</h2>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                <h3 style = {{color: 'white'}}>{this.props.tempMin} {String.fromCharCode(8457)}{String.fromCharCode(8726)} 
                    {this.props.tempMax} {String.fromCharCode(8457)}
                </h3>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '50%'}}>
            <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
        
            <FontAwesomeIcon
                    style = {sun}
                    icon = "sun"
                />
            </Col>
            </Row>
            </Container>
        	</div>	
        	
        
        )
     }
     else if (this.props.weather === 'Clouds') {
            return (
        <div>
            <Container fluid>
            <Row style = {{textAlign: 'center', maxHeight: '25%', marginTop: '7rem'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h1 style = {{color: 'white'}}>{this.props.city}</h1>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h2 style = {{color: 'white'}}>{this.props.weather}</h2>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                <h3 style = {{color: 'white'}}>{this.props.tempMin} {String.fromCharCode(84578457)}{String.fromCharCode(8726)} 
                    {this.props.tempMax} {String.fromCharCode(84578457)}
                </h3>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '50%'}}>
            <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
        
            <FontAwesomeIcon
                    style = {cloud}
                    icon = "Cloud"
                />
            </Col>
            </Row>
            </Container>
            </div>  
        )
    }
    else if (this.props.weather === 'Rain') {
            return (
         <div>
            <Container fluid>
            <Row style = {{textAlign: 'center', maxHeight: '25%', marginTop: '7rem'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h1 style = {{color: 'white'}}>{this.props.city}</h1>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h2 style = {{color: 'white'}}>{this.props.weather}</h2>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                <h3 style = {{color: 'white'}}>{this.props.tempMin} {String.fromCharCode(845784578457)}{String.fromCharCode(8726)} 
                    {this.props.tempMax} {String.fromCharCode(845784578457)}
                </h3>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '50%'}}>
            <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
        
            <FontAwesomeIcon
                    style = {rain}
                    icon = "tint"
                />
            </Col>
            </Row>
            </Container>
            </div>  
        )
    }
    else if (this.props.weather === 'Hazy') {
            return (
        <div>
            <Container fluid>
            <Row style = {{textAlign: 'center', maxHeight: '25%', marginTop: '7rem'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h1 style = {{color: 'white'}}>{this.props.city}</h1>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h2 style = {{color: 'white'}}>{this.props.weather}</h2>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                <h3 style = {{color: 'white'}}>{this.props.tempMin} {String.fromCharCode(8457845784578457)}{String.fromCharCode(8726)} 
                    {this.props.tempMax} {String.fromCharCode(8457845784578457)}
                </h3>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '50%'}}>
            <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
        
            <FontAwesomeIcon
                    style = {cloud}
                    icon = "cloud"
                />
            </Col>
            </Row>
            </Container>
            </div>  
        )
    }
    else if (this.props.weather === 'Snow') {
        return (
        <div>
            <Container fluid>
            <Row style = {{textAlign: 'center', maxHeight: '25%', marginTop: '7rem'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h1 style = {{color: 'white'}}>{this.props.city}</h1>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                    <h2 style = {{color: 'white'}}>{this.props.weather}</h2>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '25%'}}>
                <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
                <h3 style = {{color: 'white'}}>{this.props.tempMin} {String.fromCharCode(84578457845784578457)}{String.fromCharCode(8726)} 
                    {this.props.tempMax} {String.fromCharCode(84578457845784578457)}
                </h3>
                </Col>
            </Row>
            <Row style = {{textAlign: 'center', height: '50%'}}>
            <Col style = {{backgroundColor: 'rgba(23, 162, 184, 0.65)'}}>
        
            <FontAwesomeIcon
                    style = {cloud}
                    icon = "snowflake"
                />
            </Col>
            </Row>
            </Container>
            </div>  
        )
    }
    else { 
        return (
            null
        )
    }
}
}

