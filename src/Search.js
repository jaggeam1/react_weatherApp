import React from 'react';

export default class Search extends React.Component{

  

	render() {

    var ulStyle = {
      boxShadow: '0 5px 10px rgba(0,0,0,0.5)',
      backgroundColor: 'rgba(100%, 100%, 100%)',
      position: 'absolute',
      zIndex: '2'
    }

    var listStyle = {
      listStyleType: 'none',
      fontFamily: 'Arial',
      fontWeight: 'bold',
      fontSize: '0.9em',
      zIndex: '2'
    }

		return (
        <ul style = {ulStyle}>
          { this.props.predictions.map((element, i) => {
            for(var x in element) {
 				     if(x === 'description') {
   					    return (
                  <li 
                    style = {listStyle}
                    key = {i}
                    onClick = {this.props.handleClick}
                  >
                    {element[x]}

                  </li>
                )
              }

            }
          })
      }
      
     </ul>

    )
  }
}
     
