import React from 'react';
import {Container, Col, Row} from 'reactstrap';
import { InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';
import {Form} from 'reactstrap'
import {Card} from 'reactstrap'
import Search from './Search'
import Time from './Time'
import WeatherBackgroundImage from './WeatherBackgroundImage'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMapMarker } from '@fortawesome/free-solid-svg-icons'
import fontawesome from '@fortawesome/fontawesome'
import '@fortawesome/fontawesome-free'
import LocalWeatherText from './LocalWeatherText'
import Forecast from './Forecast'

fontawesome.library.add(faMapMarker)


export default class Layout extends React.Component{

render() {

var inputStyle = {
	marginTop: '2rem'
}

var rowStyle = {
	paddingBottom: '0.5em',
	zIndex: '1'
}

 var icon = {
	marginTop: '2rem',
	color: 'white',
	marginLeft: '5rem',
	marginTop: '2.5rem'
}

var text = {
        position: 'absolute',
        zIndex: '3',
        paddingTop: '15em',
        textAlign: 'center'
    }

var text1 = {
        position: 'absolute',
        zIndex: '3',
        paddingTop: '30em',
        marginLeft: '30em',
        display: this.props.display
    }

var text2 = {
        position: 'absolute',
        zIndex: '3',
        paddingTop: '30em',
        textAlign: 'center',
        display: this.props.display
    }

var text3 = {
        position: 'absolute',
        zIndex: '3',
        paddingTop: '30em',
        marginLeft: '56em',
        display: this.props.display
    }

var rowStyleForecast = {
	position: 'absolute',
	zIndex: '2',
	marginTop: '10rem'
}

var button = {
	position: 'absolute',
	zIndex: '2',
	marginTop: '40rem',
	textAlign: 'center'
}
	




return (
		<div>
			<Container>
				<Row className="justify-content-between bg-dark" style = {rowStyle}>
      				<Col>
      					<Time
      						time = {this.props.time}
      					/>
      				</Col>
      				<Col sm = "1">
						<FontAwesomeIcon
							style = {icon}
            				className = "icon"
            				icon = {'map-marker'}
                			onClick = {this.props.getLocalWeather}
            			/> 
            		</Col>
					<Col sm = "3" style = {inputStyle}>
							<Form>
								<Input 
								display = {this.props.display}
								placeholder = 'Search'
								onChange = {this.props.handleChange}
								type = "text"
								name = "placesSearch"
								value = {this.props.placesSearch}
							/>
						</Form>
					</Col>
				</Row>
				<Row className="justify-content-end">
					<Col xs="3">
						<Search
							predictions = {this.props.predictions}
							display = {this.props.display}
							handleClick = {this.props.handleClick}
							cityPlaceholder = {this.props.cityPlaceholder}
						/>
					</Col>
				
					
					
				</Row>
				<Row>
					<Col style = {button}>
        				
						<button
							onClick = {this.props.getForecast}
        				>
        					Get Forecast
        				</button>
        			</Col>
        		</Row>
				<Row style = {rowStyle}>
					<WeatherBackgroundImage
							weather = {this.props.weather}
							time = {this.props.time}
							timeStamp = {this.props.timeStamp}
					/>
					<Col style = {text}>
						<LocalWeatherText
							city = {this.props.city}
							rain = {this.props.rain}
							tempMin = {this.props.tempMin}
							tempMax = {this.props.tempMax}
							weather = {this.props.weather}
						/>
					</Col>
					<Col style = {text1}>
						<h1>{this.props.day1Month} {String.fromCharCode(8726)} {this.props.day1Day}</h1>
							<p>{this.props.day1max} {String.fromCharCode(8451)} {String.fromCharCode(8726)} {this.props.day1min} {String.fromCharCode(8451)}</p>
					</Col>
					<Col style = {text2}>
						<h1>{this.props.day2Month} {String.fromCharCode(8726)} {this.props.day2Day}</h1>
							<p>{this.props.day2max} {String.fromCharCode(8451)} {String.fromCharCode(8726)} {this.props.day2min} {String.fromCharCode(8451)}</p>
					</Col>
					<Col style = {text3}>
					
						<h1>{this.props.day3Month} {String.fromCharCode(8726)} {this.props.day3Day}</h1>
							<p>{this.props.day3max} {String.fromCharCode(8451)} {String.fromCharCode(8726)} {this.props.day3min} {String.fromCharCode(8451)}</p>
					</Col>
				</Row>
				
				
				
			</Container>
		</div>
	)
}
}