import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Input } from 'reactstrap';
import fontawesome from '@fortawesome/fontawesome'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {faMapMarker} from '@fortawesome/free-solid-svg-icons'
import Search from './Search'

fontawesome.library.add(faSearch)
fontawesome.library.add(faMapMarker)

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {

    return (
      <div>
        <Navbar color="grey" light expand="md">
          <NavbarBrand>
              <FontAwesomeIcon
                style = {{color: 'black', marginLeft: '0.2em'}}
                icon = {"map-marker"}
                onClick = {this.props.getWeather}
              />
              My Location
          </NavbarBrand>
              {this.props.time}
          <NavbarToggler>
            	<FontAwesomeIcon 
             		 onClick={this.toggle} icon = {'search'}/>
          </NavbarToggler>
        	<Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
              <Input
              	style = {{paddingLeft: '4rem'}}
                type = "text"
                name = "placesSearch"
                value = {this.props.placesSearch}
                onChange = {this.props.handleChange}
                placeholder = "Search"
              />
              <Search 
                predictions = {this.props.predictions}
                handleClick = {this.props.handleClick}
              />
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}