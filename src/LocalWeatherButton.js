import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMapMarker } from '@fortawesome/free-solid-svg-icons'
import fontawesome from '@fortawesome/fontawesome'
import '@fortawesome/fontawesome-free'

fontawesome.library.add(faMapMarker)

export default class LocalWeatherButton extends React.Component{

render() {
	return (
            <div
                onClick = {this.props.getLocalWeather}
            /> 
    )
}
}