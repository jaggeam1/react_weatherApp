Aimport React from 'react';
import {geolocated} from 'react-geolocated';
import axios from 'axios'
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

export default class App extends React.Component{

 constructor(props) {
    super(props);
    this.state = {
      Lat:'Loading',
      Long:'Loading',
      temp: '',
      city: '',
      citySearch: ''
    }
 }

 componentDidMount() {
  var that = this;
  navigator.geolocation.getCurrentPosition(
    function(position) {
    that.setState({
      Lat: position.coords.latitude,
      Long: position.coords.longitude
    });
 },

 function (error) {
},
 {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
 )
 }


getWeather(Lat, Long) {
  var APIKEY = "&appid=16909a97489bed275d13dbdea4e01f59";
  let api = "https://api.openweathermap.org/data/2.5/weather?lat=" + this.state.Lat + "&lon=" + this.state.Long + "&units=metric" + APIKEY;

  axios.get(api)
    .then((response)=> {
      debugger
      console.log('The local weather is '+ response.data.main.temp)
      console.log('Your city: '+ response.data.name)
    this.setState({temp : response.data.main.temp, city: response.data.name})
  })
}

getForecast() {
  var APIKEY = "&appid=16909a97489bed275d13dbdea4e01f59";
  let apiFore = "https://api.openweathermap.org/data/2.5/forecast?lat=" + this.state.Lat + "&lon=" + this.state.Long + APIKEY;
  debugger

  axios.get(apiFore)
    .then((response) => {
      debugger
    console.log('The local forecast is ' + response.data.list[0].main.humidity)
    })
  }

handleChange(event) {
  
  this.setState({citySearch: event.target.value });
};

handleSelect() {
  var citySearch = this.state.citySearch
  geocodeByAddress(citySearch)
    .then(results => getLatLng(results[0]))
    .then(latLng => console.log('Success', latLng))
    .catch(error => console.error('Error', error));
}

getPlace() {
  var APIKEY = "AIzaSyDvBNuUxPwXtCEjWCK5DfYiQEP4yw-KKTI"
  var input = this.refs.input.value
  let apiPlace = "https://maps.googleapis.com/maps/api/place/autocomplete/output?input=" + input + "&types=(cities)" + "&key=" + APIKEY

  axios.get(apiPlace)
    .then((response) => {
      debugger
    console.log(response.predictions.description)
    })
  }


render () {
return (
  <div>
    <div>
      <PlacesAutocomplete
        value = {this.state.citySearch}
        onChange = {this.handleChange}
        onSelect = {this.handleSelect}
      >

      {({getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div>
        <input
          {...getInputProps({
            placeholder: 'Search Places',
            className: 'location-search-input'
          })}
          type = "text"
         name = "citySearch"
         value = {this.state.citySearch}
        />
        
        <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item';
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                  : { backgroundColor: '#ffffff', cursor: 'pointer' };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
            </div>
        )}
      </PlacesAutocomplete>
      </div>

      <div>
        <button
          onClick = {this.getPlace.bind(this)}
        >
          Submit
        </button>
      </div>

      <div>
        <button
          onClick = {this.getWeather.bind(this)}
        > 
          Get local weather
        </button>
        <button
          onClick = {this.getForecast.bind(this)}
        >
          Get local forecast
        </button>
      </div>

      <div>
        <h1>{this.state.city}</h1>
          <p>{this.state.temp}</p>
      </div>

    </div>
  )
 }
}

