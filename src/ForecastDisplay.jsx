import React from 'react';
import {Container, Row, Col} from 'reactstrap'

export default class ForecastDisplay extends React.Component{

	render() {

		var style = {
			display: this.props.display
		}

		return (
			<div style = {style}>
			<Container>
				<Row>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h2 style = {{color: 'white'}}>{this.props.day1Month} / {this.props.day1Day}</h2>
					</Col>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h2 style = {{color: 'white'}}>{this.props.day2Month} / {this.props.day2Day}</h2>
					</Col>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h2 style = {{color: 'white'}}>{this.props.day3Month} / {this.props.day3Day}</h2>
					</Col>
				</Row>
				<Row>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h3 style = {{color: 'white', fontSize: '1em'}}>{this.props.day1max}{String.fromCharCode(8457)} / {this.props.day1min}{String.fromCharCode(8457)}</h3>
					</Col>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h3 style = {{color: 'white', fontSize: '1em'}}>{this.props.day2max}{String.fromCharCode(8457)} / {this.props.day2min}{String.fromCharCode(8457)}</h3>
					</Col>
					<Col style = {{backgroundColor: 'rgba(4, 35, 65, 0.61'}}>
						<h3 style = {{color: 'white', fontSize: '1em'}}>{this.props.day3max}{String.fromCharCode(8457)} / {this.props.day3min}{String.fromCharCode(8457)}</h3>
					</Col>
				</Row>
			</Container>
			</div>
		)
	}
}