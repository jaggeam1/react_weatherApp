import React from 'react';
import {Button} from 'reactstrap'
import {Container, Row, Col} from 'reactstrap'
import ForecastDisplay from './ForecastDisplay'

export default class Forecast extends React.Component{

render() {
	return(
		<Container>
			<Row style = {{textAlign: 'center'}}>
				<Col>
					<Button
						onClick = {this.props.getForecast}
					>
						Get local forecast
    				</Button>
    				<ForecastDisplay
    					day1min = {this.props.day1min}      
        				day1max = {this.props.day1max}
        				day2min = {this.props.day2min} 
        				day2max = {this.props.day2max} 
        				day3min = {this.props.day3min} 
        				day3max = {this.props.day3max} 
        				day1Month = {this.props.day1Month} 
        				day1Day = {this.props.day1Day} 
        				day2Month = {this.props.day2Month} 
        				day2Day = {this.props.day2Day} 
        				day3Month = {this.props.day3Month} 
        				day3Day = {this.props.day3Day}
        				display = {this.props.display}
        			/> 
    			</Col>
    		</Row>
    	</Container>
    )
}
}