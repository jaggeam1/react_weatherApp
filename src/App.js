import React from 'react';
import axios from 'axios'
import Header from './Header'
import LocalWeatherText from './LocalWeatherText'
import Forecast from './Forecast'


export default class App extends React.Component{

 constructor(props) {
    super(props)
    this.state = {
      display: 'none',
      lat:'Loading',
      long:'Loading',
      time: '',
      placesSearch: '',
      predictions: [''],
      placeId: '',
      tempMin: '',
      tempMax: '',
      weather: '',
      day1min: '',
      day1max: '',
      day2min: '',
      day2max: '',
      day3min: '',
      day3max: '',
      day1Month: '',
      day1Day: '',
      day2Month: '',
      day2Day: '',
      day3Month: '',
      day3Day: '',
      target: ''
    }
 }

	componentDidMount() {
    	var that = this;
    	navigator.geolocation.getCurrentPosition(
    	function(position) {
      		that.setState({
        		lat: position.coords.latitude,
        		long: position.coords.longitude
      		})
    	},
    	function (error) {
    	},
      		{enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    )
    	var time = new Date()
    	var daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"]
    	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    	var month = months[time.getMonth()]
    	var date = time.getDate()
    	var year = time.getFullYear()
    	var dayOfWeek = daysOfWeek[time.getDay()]
    	var dateString = dayOfWeek + " " + month + " " + date + ", " + year
    		this.setState({time: dateString})
  	}

  
	componentDidUpdate() {
  		var APIKEYWEATHER = "&appid=5018eaf71f13c9b665455444f35afa20";
  		let api = "https://api.openweathermap.org/data/2.5/weather?lat=" + this.state.lat + "&lon=" + this.state.long + "&units=imperial" + APIKEYWEATHER;
  		if(this.state.tempMin === '' & this.state.tempMax === '') {
  			axios.get(api)
    			.then((response)=> {
      				this.setState({tempMin : response.data.main.temp_min, tempMax : response.data.main.temp_max, city: response.data.name, clouds: response.data.clouds.all, weather: response.data.weather[0].main, timeStamp: response.data.sys.sunset})
    			})
  		}
  	}

  
	handleChange(event) {
  		event.preventDefault()
  		this.setState({placesSearch: event.target.value})
  		var APIKEYGOOGLE = "AIzaSyDvBNuUxPwXtCEjWCK5DfYiQEP4yw-KKTI"
  		let apiPlace = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + this.state.placesSearch + "&types=(cities)&offset=0&language=en&key=" + APIKEYGOOGLE
  		axios.get(apiPlace)
    		.then((response) => {
      		this.setState({predictions: response.data.predictions})
    	})
	}

	getForecast() {
  		var day1 = new Date()
  		var day2 = new Date()
  		var day3 = new Date()
  		day1.setDate(day1.getDate() + 1);
  		day2.setDate(day1.getDate() + 1);
  		day3.setDate(day1.getDate() + 2);
  		var day1Month = day1.getMonth() + 1
  		var day1Day = day1.getDate()
  		var day2Month = day2.getMonth() + 1
  		var day2Day = day2.getDate() 
  		var day3Month = day3.getMonth() + 1
  		var day3Day = day3.getDate()
  		var day1MonthStr = day1Month.toString()
  		var day1DayStr = day1Day.toString()
  		var day2MonthStr = day2Month.toString()
  		var day2DayStr = day2Day.toString()
  		var day3MonthStr = day3Month.toString()
  		var day3DayStr = day3Day.toString()
  		var APIKEYWEATHER = "&appid=5018eaf71f13c9b665455444f35afa20";
  		let apiFore = "https://api.openweathermap.org/data/2.5/forecast?lat=" + this.state.lat + "&lon=" + this.state.long +  "&units=imperial" + APIKEYWEATHER;
  		axios.get(apiFore)
    		.then((response) => {
      			this.setState({day1max: response.data.list[1].main.temp_max, day1min: response.data.list[1].main.temp_min, day2max: response.data.list[2].main.temp_max, day2min: response.data.list[1].main.temp_min, day3max: response.data.list[3].main.temp_max, day3min: response.data.list[1].main.temp_min, day1Month: day1MonthStr, day1Day: day1DayStr, day2Month: day2MonthStr, day2Day: day2DayStr, day3Month: day3MonthStr, day3Day: day3DayStr, display: 'block' })
      		})
	}

	handleClick(event){
  		event.preventDefault()
  		var target = event.target.textContent
  		this.state.predictions.map((ele) => {
    	if(ele.description === target) {
      		for(var y in ele) {
        		if(y === "place_id") {
          			var placeId = ele[y];
          				this.setState({taret: target, placeId: placeId, placesSearch: ''}, () => {
          				})
        		}
      		}
   		}
  		return this.state.placeId
  		})
  		var APIKEYGOOGLE = "AIzaSyDvBNuUxPwXtCEjWCK5DfYiQEP4yw-KKTI"
  		let apiLatLon = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + this.state.placeId + "&key=" + APIKEYGOOGLE;
  		axios.get(apiLatLon)
    		.then((response) => {
      			var cityPlaceholder = response.data.result
      				for(var geo in cityPlaceholder) {
        				if (geo === "geometry") {
            				for(var loc in cityPlaceholder[geo]) {
                				for (var lat in cityPlaceholder[geo][loc]) {
                  					if (lat === "lat"){
                  						this.setState({lat: response.data.result.geometry.location.lat})
                  							for(var lng in cityPlaceholder[geo][loc]) {
                    				if (lng === "lng") {
                      					this.setState({long: response.data.result.geometry.location.lng})
                    				}
                  					}
                  				}
                			}		
                    	}
                	}
                }
            })
    }
  
	componentDidUpdate() {
  		var APIKEYWEATHER = "&appid=16909a97489bed275d13dbdea4e01f59";
  		let api = "https://api.openweathermap.org/data/2.5/weather?lat=" + this.state.lat + "&lon=" + this.state.long + "&units=imperial" + APIKEYWEATHER;
  		if(this.state.city !== this.state.target) {
  		axios.get(api)
    		.then((response)=> {
      			this.setState({tempMin : response.data.main.temp_min, tempMax : response.data.main.temp_max, city: response.data.name, clouds: response.data.clouds.all, weather: response.data.weather[0].main, timeStamp: response.data.sys.sunset})
    		})
  		}
	}

  getWeather() {
  var that = this;
    navigator.geolocation.getCurrentPosition(
    function(position) {
      that.setState({
        lat: position.coords.latitude,
        long: position.coords.longitude
      })
    },
    function (error) {
    },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    )
  var APIKEYWEATHER = "&appid=5018eaf71f13c9b665455444f35afa20";
  let api = "https://api.openweathermap.org/data/2.5/weather?lat=" + this.state.lat + "&lon=" + this.state.long + "&units=metric" + APIKEYWEATHER;
  axios.get(api)
    .then((response)=> {
      this.setState({tempMin : response.data.main.temp_min, tempMax : response.data.main.temp_max, city: response.data.name, clouds: response.data.clouds.all, weather: response.data.weather[0].main})
    })
  }
 

render () {
  return (
    <div>
      <div>
      <Header
        handleChange = {this.handleChange.bind(this)}
        type = "text"
        name = "placesSearch"
        value = {this.state.placesSearch}
        predictions = {this.state.predictions}
        time = {this.state.time}
        getWeather ={this.getWeather.bind(this)}
      	handleClick = {this.handleClick.bind(this)}
      />
      </div>

      <div>
      <LocalWeatherText
        city = {this.state.city}
        tempMax = {this.state.tempMax}
        tempMin = {this.state.tempMin}
        weather = {this.state.weather}
      />
      <Forecast
        getForecast = {this.getForecast.bind(this)}
        lat = {this.state.lat}
        long = {this.state.long}
        day1min = {this.state.day1min}      
        day1max = {this.state.day1max}
        day2min = {this.state.day2min} 
        day2max = {this.state.day2max} 
        day3min = {this.state.day3min} 
        day3max = {this.state.day3max} 
        day1Month = {this.state.day1Month} 
        day1Day = {this.state.day1Day} 
        day2Month = {this.state.day2Month} 
        day2Day = {this.state.day2Day} 
        day3Month = {this.state.day3Month} 
        day3Day = {this.state.day3Day}
        display = {this.state.display} 

      />
      </div>
    </div>
  )
 }
}


